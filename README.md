This dotCMS plugin enables XML sitemap generation in dotCMS.

## Configuration
XML Sitemap configuration is done in the file `conf/dotmarketing-config-ext.properties`. See the [dotCMS documentation on XML Sitemaps](https://dotcms.com/docs/latest/xml-sitemap) for details.

### Job Scheduling
The XML Sitemap functionality built into dotCMS periodically compiles the XML sitemaps based on dotCMS's data. The timing of this job can be controlled with the property `org.dotcms.XMLSitemap.CRON_EXPRESSION`. The value of this property should be a Quartz Cron Expression. For more information on Quartz's cron expressions, see the (Quartz CronTrigger tutorial](http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/tutorial-lesson-06.html).